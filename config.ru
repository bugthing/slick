# frozen_string_literal: true

SlickEnv = Class.new(Struct.new(:env)) do
  def production?
    env == "production"
  end

  def development?
    env == "development"
  end
end.new(ENV.fetch("SLICK_ENV", "production"))

require "bundler/setup"
Bundler.require

loader = Zeitwerk::Loader.new
loader.push_dir("#{__dir__}/lib")

if SlickEnv.development?
  Bundler.require(:development)
  loader.enable_reloading
  listener = Listen.to("#{__dir__}/lib") do |modified, added, removed|
    loader.reload
  end
  listener.start
end
loader.setup

Rackup::Server.start(
  app: Slick::App.new,
  Port: ENV.fetch("PORT", 5000)
)
