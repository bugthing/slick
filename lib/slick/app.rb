# frozen_string_literal: true

module Slick
  class App
    def call(env)
      [200, {"Content-Type" => "text/plain"}, [Slick::Content.call]]
    end
  end
end
