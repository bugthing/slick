# frozen_string_literal: true

module Slick
  class Content
    def self.call
      "Hello Dave\n"
    end
  end
end
