# The little http server that might

Here is a small rack application that excepts http requests in an effort to see how fast we can go.

## Setup

Install truffle ruby and bundler I did this:

      asdf install ruby truffleruby+graalvm-22.3.1
      gem install bundler

If you have truffle ruby, you'll need to fetch the supporting libs

      bundle install

## Usage

Spin the application up.

      make serve

Make lots of requests and measure

      curl http://0.0.0.0:5000

