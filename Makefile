.DEFAULT_GOAL := serve

serve:
	bundle exec rackup --server falcon

dev:
	SLICK_ENV=development bundle exec rackup --server falcon
